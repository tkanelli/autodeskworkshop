﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlaceBalls : MonoBehaviour {

	[SerializeField] List<GameObject> m_PlacedPrefabs;
	[SerializeField] List<GameObject> m_PlacedCubes;
	[SerializeField] List<GameObject> m_PlacedCylinder;

    //Three types of objects to spawn, added manually. 
    //Add additional object names to spawn other types of objects.
    [SerializeField]
	public enum PlacementType
	{
		Cubes,
		Spheres,
		Cylinders
	};

	public PlacementType TypetoPlace = PlacementType.Spheres;
	
	[SerializeField] Transform m_PlacementTransform;

    //Selects a random integer 
	int m_RandomMax;

	bool m_TurnedOffAnim = false;

	public Transform placementTransform
	{
		get { return m_PlacementTransform; }
		set { m_PlacementTransform = value; }
	}
	
	public GameObject spawnedObject { get; private set; }
	
	FocusCircle m_FocusCircle;
	int m_IndexToPlace;
	int m_NextIndex;

	GameObject m_ObjectToPlace;

    //First we get the FocusCircle transform, so we can spawn our objects at that location
	void Awake()
	{
		m_FocusCircle = GetComponent<FocusCircle>();
		placementTransform = m_FocusCircle.FocusCirclePosition;
		m_RandomMax = m_PlacedPrefabs.Count;
		m_IndexToPlace = Random.Range(0, m_PlacedPrefabs.Count);
	}

    //spawns the object based on the current state
    //Separated from Update function so we can call with screen touch and with buttons (for debugging and rapid prototyping).
    public void SpawnObject()
    {
        m_ObjectToPlace = GetObjectToPlace();

        //Clone the object selected, and set it as the spawned object
        spawnedObject = Instantiate(m_ObjectToPlace, m_PlacementTransform.position,
            m_PlacementTransform.rotation);

        if (!m_TurnedOffAnim)
        {
            UIManager2 m_Manager = GetComponent<UIManager2>();
            if (m_Manager)
            {
                m_Manager.tapToPlaceGameObject.SetActive(false);
            }
        }
    }

    void Update ()
	{
        var touch = Input.GetTouch(0);

        if (Input.touchCount == 0)
		{
			return;
		}

        //Don't do anything if tapping UI
		if(IsTouchOverUIObject(touch))
		{
			return;
		}

		if (touch.phase == TouchPhase.Began)
		{
			if (m_PlacementTransform != null && m_FocusCircle.onPlane)
			{

                SpawnObject();

				//GetNextIndex(); //Deprecated
			}
		}
		
	}

	void GetNextIndex()
	{
		m_NextIndex = Random.Range(0, m_PlacedPrefabs.Count);
		if (m_NextIndex != m_IndexToPlace)
		{
			m_IndexToPlace = m_NextIndex;
		}
		else
		{
			GetNextIndex();
		}
	}

    //Makes button numbers correspond to object spawn options' indices (0 : Cubes, 1 : Spheres, 2: Cylinders)
    public void SwitchPlacementType(int enumVal)
	{
		TypetoPlace = (PlacementType)enumVal;
	}

    //Places objects, based on the IEnumerator options (above)
    GameObject GetObjectToPlace()
	{
		GameObject retVal = null;
		
		switch (TypetoPlace)
		{
			case PlacementType.Cubes:
				retVal = m_PlacedCubes[0];	
				break;
			
			case PlacementType.Spheres:
                //retVal = m_PlacedPrefabs[m_IndexToPlace]; //This line randomizes the spheres that are spawned, if there are more then 1 in the list
                retVal = m_PlacedPrefabs[0]; break;
			
			case PlacementType.Cylinders:
				retVal = m_PlacedCylinder[0];
				break;
		}

		return retVal;
	}

    //Ensures we don't interact with scene if touching with UI
    bool IsTouchOverUIObject(Touch touch)
	{   
		PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
		eventDataCurrentPosition.position = touch.position;
		List<RaycastResult> results = new List<RaycastResult>();
		EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
		return results.Count > 0;
	}
}
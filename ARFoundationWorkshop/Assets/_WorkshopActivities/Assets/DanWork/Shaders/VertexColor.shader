﻿Shader "Custom/VertexColor" {

    Properties {
        _RampTex ("Texture", 2D) = "white" {}
    }

	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert vertex:vert
		#pragma target 3.0

		struct Input {
			float4 vertColor;
			float2 uv_RampTex;
		};
		
		sampler2D _RampTex;

		void vert(inout appdata_full v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.vertColor = v.color;
		}

		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = IN.vertColor.rgb;
			//o.Albedo = tex2D (_RampTex, IN.uv_RampTex).rgb;
			//o.Albedo *= IN.vertColor.rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

//using UnityEngine.XR.iOS;

public class FocusCircle : MonoBehaviour
{
    //animations to modify object visibility (simulating on/off) when detecting a plane
	const string ANIM_FADEON = "FadeOn";
	const string ANIM_FADEOFF = "FadeOff";
	
	bool m_OnPlane = false; //if plane is detected
	
    //bool m_ProbesFound = false; //Deprecated
	
    bool m_CanPlace = true; //enabling object placement

	public bool onPlane
	{
		get { return m_OnPlane; }
	}
	
	//[SerializeField] float m_PlaneFindingDist = 0.5f; //Finds planes within .5m? No references in project

	[SerializeField] GameObject m_PlacementCircle; //The target that snaps to planes, and aims the object placement
	[SerializeField] Animator m_OuterCircleAnim; //Outside of the placement circle

	
	[SerializeField] GameObject m_ScreenspaceCenter; //
	[SerializeField] TrackableType m_FocusCircleTrackableType = TrackableType.PlaneWithinPolygon; //

	Vector2 m_CenterScreen;
	ARSessionOrigin m_SessionOrigin;
	ARPlaneManager m_ARPlaneManager;
	static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();
	static List<ARPlane> s_Planes = new List<ARPlane>();
	bool m_PortraitMode = false;

	[SerializeField]
	ARRaycastManager m_RaycastManager;
	
	public Transform FocusCirclePosition
	{
		get { return m_PlacementCircle.transform; }
	}

	void Awake()
	{
		m_SessionOrigin = FindObjectOfType<ARSessionOrigin>();
		m_ARPlaneManager = FindObjectOfType<ARPlaneManager>();

		m_PortraitMode = (Input.deviceOrientation == DeviceOrientation.Portrait ||
		                  Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown);
	}

	void Start()
	{
		m_PlacementCircle.SetActive(false);
		m_ScreenspaceCenter.SetActive(false);
		
		m_CenterScreen = new Vector2(Screen.width / 2, Screen.height / 2);
	}
	
	// Update is called once per frame
	void Update () {
		
		// recalculate the center when device orientation changes
		if (m_PortraitMode != (Input.deviceOrientation == DeviceOrientation.Portrait ||
		                       Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown))
		{
			m_CenterScreen = new Vector2(Screen.width / 2, Screen.height / 2);
			m_PortraitMode = !m_PortraitMode;
		}
		

		if (PlanesFoundAndTracking())
		{
			m_PlacementCircle.SetActive(true);
			if (m_RaycastManager.Raycast(m_CenterScreen, s_Hits, m_FocusCircleTrackableType))
			{
				// turn off Sc circle, fade on focus circle, snap focus circle to plane
				m_ScreenspaceCenter.SetActive(false);

                m_OuterCircleAnim.SetBool(ANIM_FADEON, true);

				Pose m_FirstPlanePose = s_Hits[0].pose;

				m_PlacementCircle.transform.position = m_FirstPlanePose.position;
				m_PlacementCircle.transform.rotation = m_FirstPlanePose.rotation;
				m_OnPlane = true;
			}
			else
			{
				m_PlacementCircle.SetActive(false);
				m_ScreenspaceCenter.SetActive(true);
				m_OnPlane = false;
			}
		}

	}
	
	public Vector3 GetCursorPOS()
	{
		return m_PlacementCircle.transform.position;
	}

	bool PlanesFoundAndTracking()
	{
		return m_ARPlaneManager.trackables.count > 0;		
	}
}

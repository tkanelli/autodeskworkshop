﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Author: Tim Kanellitsas, KairosXR
///
/// Responsibilities:
/// 1. Toggle activation between the two UI buttons used to switch state between Manipulation and Object Spawning.
///
/// How to use:
/// Add it to an empty gameobject. We called the empty gameobject "UI Visibility Controller"
/// </summary>

public class UIVisibilityController : MonoBehaviour
{
    public SpawnedObjectManager spawnedObjectManager;
    public GameObject manipulationUI, spawningUI;


    private void Update()
    {
        if(spawnedObjectManager.interactionState == SpawnedObjectManager.InteractionState.ObjectManipulation)
        {
            manipulationUI.SetActive(true);
            spawningUI.SetActive(false);
        }
        else
        {
            manipulationUI.SetActive(false);
            spawningUI.SetActive(true);
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Author: Tim Kanellitsas, KairosXR
///
/// Responsibilities:
/// Swaps the materials of the currently selected object (in the SpawnedObjectManager)
///
/// How to use:
/// Add it to an empty gameobject. We called the empty gameobject "Object Material Manager"
/// </summary>

public class ObjectMaterialManager : MonoBehaviour
{
    public SpawnedObjectManager spawnedObjectManager; //public reference so the currently selected object is known.

    public Material highlightMat; //public reference for custom material.

    void Update()
    {
        HandleMaterials();
    }

    //Swap the material of spawnedObjectManager.currObjectRenderer if currObject != null
    public void HandleMaterials()
    {
        if(spawnedObjectManager.currObject != null)
        {
            spawnedObjectManager.currObjectRenderer.material = highlightMat;
        }
    }
}
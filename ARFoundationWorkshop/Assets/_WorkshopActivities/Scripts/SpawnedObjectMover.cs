﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Author: Tim Kanellitsas, KairosXR
///
/// Responsibilities:
/// 1.  Moves the currently selected object, based on the user's raycasts when they tap the screen
/// 2.  Cancels positioning functionality if user is tapping UI
///
/// How to use:
/// Add it to an empty gameobject. We called the empty gameobject "Spawned Object Mover"
/// </summary>

public class SpawnedObjectMover : MonoBehaviour
{
    Ray raycast;
    RaycastHit hit;

    int layerMask = 1 << 9; //Makes it so the user cannot
    
    public SpawnedObjectManager spawnedObjectManager; //Direct reference so the spawned object mover knows which object to move

    private int fingerID = -1;

    //Looks for the first contact with finger, if not in editor
    private void Awake()
    {
#if !UNITY_EDITOR
     fingerID = 0; 
#endif
    }

    //Set the layer mask, so the user cannot move furniture on top of other furniture
    private void Start()
    {
        layerMask = ~layerMask;
    }

    //Always look to move objects if in correct state
    private void Update()
    {
        if(spawnedObjectManager.interactionState == SpawnedObjectManager.InteractionState.ObjectManipulation)
        {
            if (!EventSystem.current.IsPointerOverGameObject(fingerID))
            {
                ScreenInteractionHandler();
            }
        }
    }

    //Listens for "mouse clicks" or "screen taps," enabling in-editor testing on the Debug Cube
    public void ScreenInteractionHandler()
    {
        if (Application.isEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                MoveCurrObject();
            }
        }

        if (Application.isMobilePlatform)
        {
            if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began))
            {
                MoveCurrObject();
            }
        }
    }

    //Moves the currObject (selected object) based on the hit point
    public void MoveCurrObject()
    {
        if (spawnedObjectManager.currObject != null)
        {
            if (Application.isEditor)
            {
                raycast = Camera.main.ScreenPointToRay(Input.mousePosition);
            }

            if (Application.isMobilePlatform)
            {

                raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            }

            if (Physics.Raycast(raycast, out hit, 10000, layerMask))
            {
                Debug.Log("name of collided object is" + hit.collider.name);
                if (spawnedObjectManager.currObject != null)
                {
                    spawnedObjectManager.currObject.transform.position = hit.point;
                }
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

/// <summary>
/// Author: Tim Kanellitsas, KairosXR
///
/// Responsibilities:
/// 1. Plays the corresponding "Open" and "Close" Timelines, used to animate the buttons open and closed.
///
/// How to use:
/// 1. Add it to an empty gameobject. We called the empty gameobject "UI Button Animation Controller"
/// 2. Set the public references to the corresponding Open/Close Timeline sequences.
/// 3. Set the Home Button to ActivateButton().
/// 
/// </summary>


public class UIButtonAnimationController : MonoBehaviour
{
    public PlayableDirector openTimeline;
    public PlayableDirector closeTimeline;
    public bool isOpen;

    private void Start()
    {
        isOpen = false;
    }

    public void ActivateButton()
    {
        Debug.Log("activate buttons called");

        if (isOpen == false)
        {
            Debug.Log("opentimeline should play");
            OpenButtons();
            return;
        }
        else
        {
            Debug.Log("closetimeline should play");
            CloseButtons();
            return;
        }
    }

    public void CloseButtons()
    {
        closeTimeline.Play();
        isOpen = false;
    }

    public void OpenButtons()
    {
        openTimeline.Play();
        isOpen = true;
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Author: Tim Kanellitsas, KairosXR
///
/// Responsibilities:
/// 1. Spawns an object, depending on application's state and if there is an intersection with a plane.
/// 2. 
///
/// How to use:
/// Add it to an empty gameobject. We called the empty gameobject "ObjectSpawner"
/// </summary>

public class ObjectSpawner : MonoBehaviour
{
    //[Header("Set at Runtime")]
    public GameObject objectToSpawn; //The prefab that will be spawned depending on selected index in list, ObjectsToSpawn
     public GameObject spawnedObject; //The prefab clone that was recently spawned.

    Ray raycast;
    RaycastHit hit;
    int layerMask = 1 << 9;
    private int fingerID = -1;

    [Header("Set Manually")]
    public SpawnedObjectManager spawnedObjectManager;
    public List<GameObject> objectsToSpawnList = new List<GameObject>();

    private void Awake()
    {
#if !UNITY_EDITOR
     fingerID = 0; 
#endif
    }

    private void Start()
    {
        objectToSpawn = objectsToSpawnList[0];
    }

    //check to see if screen touch is over a UI object
    private void Update()
    {
        if (spawnedObjectManager.interactionState == SpawnedObjectManager.InteractionState.ObjectSpawning)
        {
            if (!EventSystem.current.IsPointerOverGameObject(fingerID))
            {
                SpawnObject();
            }
        }
    }

    //If not over a UI element, call the TouchScreenToPlane method (below)
    public void SpawnObject()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {


            if (Application.isEditor)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    TouchScreenToPlane();
                }
            }

            if (Application.isMobilePlatform)
            {
                if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began))
                {
                    TouchScreenToPlane();
                }
            }
        }
    }

    
    //Raycast from screen touch/mouse to intersection with collider, and instantiate an object
    public void TouchScreenToPlane()
    {
        if (Application.isEditor)
            {
                raycast = Camera.main.ScreenPointToRay(Input.mousePosition);
            }

            if (Application.isMobilePlatform)
            {

                raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            }

            if (Physics.Raycast(raycast, out hit))
            {
                if (spawnedObjectManager.currObject == null)
                {
                    spawnedObject = Instantiate(objectToSpawn, hit.point, hit.transform.rotation);
                }
            }
        }
    

    //Sets the item in list as the object to spawn
    public void SetObjectType(int objectIndex)
    {
        if ((objectIndex <= objectsToSpawnList.Count) && (objectIndex > -1))
        {
            objectToSpawn = objectsToSpawnList[objectIndex];
        }
    }
}
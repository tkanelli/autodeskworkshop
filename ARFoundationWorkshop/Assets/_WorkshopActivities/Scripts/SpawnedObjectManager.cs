﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Author: Tim Kanellitsas, KairosXR
///
/// Responsibilities:
/// 1. Brain of the application
/// 2. Determines if the app is in the ObjectManipulation state or the ObjectSpawning state, which controls if the user can spawn or re-position objects
/// 3. Toggles on the lean components, if they are on the currently selected object. This keeps the user from scaling and rotating other scene objects, and only manipulating the currently selected object.
/// 4. 
/// 2.  Cancels positioning functionality if user is tapping UI
///
/// How to use:
/// Add it to an empty gameobject. We called the empty gameobject "Spawned Object Manager"
/// </summary>

public class SpawnedObjectManager : MonoBehaviour
{
    Ray raycast;
    RaycastHit raycastHit;

    //public references to the selected object's lean
    [HideInInspector] public Lean.Touch.LeanTwistRotateAxis leanTwistRotateAxis;
    [HideInInspector] public Lean.Touch.LeanPinchScale leanPinchScale;

    [Header("Set Before Runtime")]
    public Material currObjectMat; //public material, so we swap out the materials when we select the object

    //[Header("Set At Runtime")]
    [HideInInspector] public GameObject currObject; //reference to the "curr object," to store the object upon selection
    [HideInInspector] public Renderer currObjectRenderer; //reference to the selected object's renderer, so the app swaps its material
    [HideInInspector] public Material currObjectOriginalMaterial; //reference to the original material, so teh app adds the old material when the currObject is deselected.

    //Two states, to keep the user from spawning and re-positioning objects at the same time
    public enum InteractionState
    {
        ObjectSpawning,
        ObjectManipulation
    };

    //public reference to the states, so we can manually switch the states in the Editor
    public InteractionState interactionState;

    //Start with ObjectSpawning, as the default. 
    private void Start()
    {
        interactionState = InteractionState.ObjectSpawning;
    }

    //Select the current object and highlight the current object if the user is not pressing an interactive UI feature.
    private void Update()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            SelectObject();
            HighlightObject();
        }
    }

    //Raycast out with mouse click or screen touch, depending on platform type
    public void SelectObject()
    {
        if (Application.isEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                raycast = Camera.main.ScreenPointToRay(Input.mousePosition);

                RayToSelectObject();
            }
        }

        if (Application.isMobilePlatform)
        {
            if ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began))
            {
                raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

                RayToSelectObject();
            }
        }
    }

    //Behavior if the raycast intersects with a virtual object.
    public void RayToSelectObject()
    {
        if (Physics.Raycast(raycast, out raycastHit))
        {
            //if raycast collides with the selected GO
            if (raycastHit.collider.gameObject == currObject)
            {
                Debug.Log(raycastHit.collider.name);
                currObject = null;
                ResetCurrObjectInfo();
                return;
            }

            //if raycast collides with a spawned object and another object is not selected
            if (raycastHit.collider.CompareTag("SpawnedObject") && currObject == null)
            {
                currObject = raycastHit.collider.gameObject;
                currObjectRenderer = currObject.GetComponent<Renderer>();
                currObjectOriginalMaterial = currObjectRenderer.material;
                GrabLeanScripts();
                ToggleOnLeanScripts();
            }

            //if raycast collides with one spawned object and another object is selected
            if (raycastHit.collider.CompareTag("SpawnedObject") && currObject != null && raycastHit.collider.gameObject != currObject)
            {
                currObjectRenderer.material = currObjectOriginalMaterial;
                ResetCurrObjectInfo();
                currObject = raycastHit.collider.gameObject;
                currObjectRenderer = currObject.GetComponent<Renderer>();
                currObjectOriginalMaterial = currObjectRenderer.material;
                GrabLeanScripts();
                ToggleOnLeanScripts();
            }
        }
    }

    //Grab the lean scale and lean rotate scripts so we can enable and disable them. 
    public void GrabLeanScripts()
    {
        leanTwistRotateAxis = currObject.GetComponent<Lean.Touch.LeanTwistRotateAxis>();
        leanPinchScale = currObject.GetComponent<Lean.Touch.LeanPinchScale>();
    }

    //Turn On the lean scripts
    public void ToggleOnLeanScripts()
    {
        leanTwistRotateAxis.enabled = true;
        leanPinchScale.enabled = true;
    }

    //Turn Off the lean scripts
    public void ToggleOffLeanScripts()
    {
        leanTwistRotateAxis.enabled = false;
        leanPinchScale.enabled = false;
    }

    //Reset the currObjectInfo (collected above) if we deselect the object
    public void ResetCurrObjectInfo()
    {
        Debug.Log("Reset currObject");
        currObjectRenderer.material = currObjectOriginalMaterial;
        ToggleOffLeanScripts();
        leanTwistRotateAxis = null;
        leanPinchScale = null;
        currObject = null;
    }

    //Highlight the currently selected object
    public void HighlightObject()
    {
        if (currObject != null)
        {
            currObjectRenderer.material = currObjectMat;
        }
    }

    //Switch states, so we can use a UI button to toggle between object spawning and object manipulation
    public void SwitchStates()
    {
        if (interactionState == InteractionState.ObjectSpawning)
        {
            interactionState = InteractionState.ObjectManipulation;
        }
        else
        {
            interactionState = InteractionState.ObjectSpawning;
        }
    }
}